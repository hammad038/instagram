import React, {useState} from 'react';
import {View, Text, StyleSheet, TouchableOpacity, TextInput, Pressable, Alert} from 'react-native';
import * as Yup from 'yup';
import {Formik} from 'formik';
import Validator from 'email-validator';
import {firebase, db} from '../../firebase';

const SignupForm = ({navigation}) => {
    const signupFormSchema = Yup.object().shape({
        email: Yup.string().email().required('Phone number / Email is required.'),
        username: Yup.string().required().min(2, 'Username has to have at least 2 characters.'),
        password: Yup.string().required().min(6, 'Your password has to have at least 6 characters.'),
    });

    const getRandomProfilePicture = async () => {
      const user = await fetch('https://randomuser.me/api');
      const userData = await user.json();
      return userData.results[0].picture.large;
    }

    const onSignup = async (email, password, username) => {
        try {
            const authUser = await firebase.auth().createUserWithEmailAndPassword(email, password);
            console.log('firebase new user created successfully...', email, password);
            db.collection('users').doc(authUser.user.email).set({
                owner_uid: authUser.user.uid,
                username,
                email: authUser.user.email,
                profile_picture: await getRandomProfilePicture(),
            })
        } catch (error) {
            Alert.alert('Sorry!', error.message);
        }
    }

    return (
        <Formik
            initialValues={{email: '', username: '', password: ''}}
            onSubmit={(inputValues) => {
                console.log(inputValues);
                onSignup(inputValues.email, inputValues.password, inputValues.username);
            }}
            validationSchema={signupFormSchema}
            validateOnMount={true}
        >
            {({handleBlur, handleChange, handleSubmit, values, errors, isValid}) => (
                <>
                    <View style={
                        [
                            styles.signupFormInputsContainer,
                            {
                                borderColor: values.email.length < 1 || Validator.validate(values.email)
                                    ? '#ccc' : 'red'
                            }
                        ]}
                    >
                        <TextInput
                            placeholder='Phone number / Email'
                            placeholderTextColor='#444'
                            autoCapitalize='none'
                            keyboardType='email-address'
                            textContentType='emailAddress'
                            autoFocus={true}
                            onChangeText={handleChange('email')}
                            onBlur={handleBlur('email')}
                            value={values.email}
                            style={styles.signupFormInputsFields}
                        />
                    </View>

                    <View style={
                        [
                            styles.signupFormInputsContainer,
                            {
                                borderColor: values.username.length < 1 || values.username.length >= 2
                                    ? '#ccc' : 'red'
                            }
                        ]}
                    >
                        <TextInput
                            placeholder='Username'
                            placeholderTextColor='#444'
                            autoCapitalize='none'
                            onChangeText={handleChange('username')}
                            onBlur={handleBlur('username')}
                            value={values.username}
                            style={styles.signupFormInputsFields}
                        />
                    </View>

                    <View style={
                        [
                            styles.signupFormInputsContainer,
                            {
                                borderColor: 1 > values.password.length || values.password.length >= 6
                                    ? '#ccc' : 'red'
                            }
                        ]}
                    >
                        <TextInput
                            placeholder='Password'
                            placeholderTextColor='#444'
                            autoCapitalize='none'
                            autoCorrect={false}
                            secureTextEntry={true}
                            textContentType='password'
                            style={styles.signupFormInputsFields}
                            onChangeText={handleChange('password')}
                            onBlur={handleBlur('password')}
                            value={values.password}
                        />
                    </View>

                    <Pressable style={styles.signupFormButton(isValid)} onPress={handleSubmit}>
                        <Text style={styles.signupFormButtonText}>Sign Up</Text>
                    </Pressable>

                    <View style={styles.signupFormLogInContainer}>
                        <Text style={{color: 'black'}}>Already have an account?{' '}</Text>
                        <TouchableOpacity onPress={() => navigation.goBack()}>
                            <Text style={{color: '#6BB0F5'}}>Login In</Text>
                        </TouchableOpacity>
                    </View>
                </>
            )}
        </Formik>
    );
};

const styles = StyleSheet.create({
    signupFormInputsContainer: {
        marginVertical: 8,
        borderWidth: 1,
        borderRadius: 4,
    },
    signupFormInputsFields: {
        padding: 12,
    },
    signupFormButton: (isValid) => ({
        borderRadius: 4,
        minHeight: 42,
        backgroundColor: isValid ? '#0096F6' : '#9ACAF7',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 20
    }),
    signupFormButtonText: {
        fontSize: 20,
        color: 'white',
    },
    signupFormLogInContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginTop: 24,
    },
});

export default SignupForm;