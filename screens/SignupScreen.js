import React from 'react';
import {SafeAreaView, StyleSheet, View, Text, Image} from 'react-native';
import SignupForm from '../components/signup/SignupForm';

const SignupScreen = ({navigation}) => {
    return (
        <SafeAreaView style={styles.signupScreenWrapper}>
            <View style={styles.signupScreenContainer}>
                <Image source={require('../assets/images/instagram-logo.png')} style={styles.signupScreenLogo} />
            </View>
            <View>
                <SignupForm navigation={navigation} />
            </View>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    signupScreenWrapper: {
        backgroundColor: 'white',
        flex: 1,
        paddingHorizontal: 12,

    },
    signupScreenContainer: {
        paddingTop: 80,
        paddingBottom: 50,
        alignItems: 'center',
    },
    signupScreenLogo: {
        width: 80,
        height: 80,
    },
});

export default SignupScreen;