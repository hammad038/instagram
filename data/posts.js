import {Users} from './users';

export const Posts = [
    {
        imageUrl: "https://f1.media.brightcove.com/8/3027992093001/3027992093001_4250021423001_gigi-vogueoz.jpg?pubId=3027992093001&videoId=4250010780001",
        userName: Users[1].user,
        user: Users[0].slug,
        profilePicture: Users[0].image,
        caption: 'This Pic Need Any Caption!',
        likes: 54,
        comments: [
            {
                user: 'some_random_guy',
                comment: 'Wow!! Girl very nice picture, where you have taken this picture. I love it.'
            },
            {
                user: 'hike_leo9',
                comment: 'I Don\'t Know What to Do. Wow!! Girl very nice picture, where you have taken this picture. I love it. Girl very nice picture, where you have taken this picture. I love it.',
            },
            {
                user: 'prince_of_persia',
                comment: 'Nice Picture!!!'
            },
            {
                user: 'happy_forever_0803',
                comment: 'Nice!!!'
            }
        ],
    },
    {
        imageUrl: "https://c4.wallpaperflare.com/wallpaper/139/0/16/hailey-baldwin-adidas-celebrities-girls-wallpaper-preview.jpg",
        userName: Users[1].user,
        user: Users[1].slug,
        profilePicture: Users[1].image,
        caption: 'Life is Like!',
        likes: 5334,
        comments: [
            {
                user: 'i_dont_0803',
                comment: 'Nice!!!'
            }
        ],
    },
    {
        imageUrl: "https://c4.wallpaperflare.com/wallpaper/107/306/418/amanda-cerny-girls-model-hd-wallpaper-preview.jpg",
        userName: Users[3].user,
        user: Users[3].slug,
        profilePicture: Users[3].image,
        caption: 'I Don\'t Know What to Do. Wow!! Girl very nice picture, where you have taken this picture. I love it. Wow!! Girl very nice picture, where you have taken this picture. I love it. Wow!! Girl very nice picture, where you have taken this picture. I love it.',
        likes: 21,
        comments: [
            {
                user: 'happy_forever_0803',
                comment: 'Nice!!!'
            }
        ],
    },
    {
        imageUrl: "https://c4.wallpaperflare.com/wallpaper/784/230/551/maxim-guselnikov-women-portrait-looking-at-viewer-wallpaper-preview.jpg",
        userName: Users[4].user,
        user: Users[4].slug,
        profilePicture: Users[4].image,
        caption: 'Tell Something Good!!!',
        likes: 123,
        comments: [
            {
                user: 'happy_forever_0803',
                comment: 'Nice!!!'
            }
        ],
    },
];