import React, {useState} from 'react';
import {View, Text, StyleSheet, TouchableOpacity, TextInput, Pressable, Alert} from 'react-native';
import * as Yup from 'yup';
import {Formik} from 'formik';
import Validator from 'email-validator';
import {firebase, db} from '../../firebase';

const LoginForm = ({navigation}) => {
    const loginFormSchema = Yup.object().shape({
        email: Yup.string().email().required('Phone number / Email / Username is required.'),
        password: Yup.string().required().min(6, 'Your password has to have at least 6 characters.'),
    });
    
    const onLogin = async (email, password) => {
      try {
          await firebase.auth().signInWithEmailAndPassword(email, password);
          console.log('firebase login successful...', email, password);
      } catch (error) {
          Alert.alert(
              'Sorry!',
              error.message + '\n\nWhat would you like to do now!',
              [
                  {
                      text: 'OK',
                      onPress: () => console.log('OK'),
                      style: 'cancel'
                  },
                  {
                      text: 'Sign Up',
                      onPress: () => navigation.push('SignupScreen')
                  }
              ]
          );
      }
    }

    return (
        <Formik
            initialValues={{email: '', password: ''}}
            onSubmit={(inputValues) => {
                console.log(inputValues);
                onLogin(inputValues.email, inputValues.password);
            }}
            validationSchema={loginFormSchema}
            validateOnMount={true}
        >
            {({handleBlur, handleChange, handleSubmit, values, errors, isValid}) => (
                <>
                    <View style={
                        [
                            styles.loginFormInputsContainer,
                            {
                                borderColor: values.email.length < 1 || Validator.validate(values.email)
                                    ? '#ccc' : 'red'
                            }
                        ]}
                    >
                        <TextInput
                            placeholder='Phone number / Email / Username'
                            placeholderTextColor='#444'
                            autoCapitalize='none'
                            keyboardType='email-address'
                            textContentType='emailAddress'
                            autoFocus={true}
                            onChangeText={handleChange('email')}
                            onBlur={handleBlur('email')}
                            value={values.email}
                            style={styles.loginFormInputsFields}
                        />
                    </View>

                    <View style={
                        [
                            styles.loginFormInputsContainer,
                            {
                                borderColor: 1 > values.password.length || values.password.length >= 6
                                    ? '#ccc' : 'red'
                            }
                        ]}
                    >
                        <TextInput
                            placeholder='Password'
                            placeholderTextColor='#444'
                            autoCapitalize='none'
                            autoCorrect={false}
                            secureTextEntry={true}
                            textContentType='password'
                            style={styles.loginFormInputsFields}
                            onChangeText={handleChange('password')}
                            onBlur={handleBlur('password')}
                            value={values.password}
                        />
                    </View>

                    <View style={{alignItems: 'flex-end', marginBottom: 30,}}>
                        <Text style={{color: '#6BB0F5'}}>Forgot password ?</Text>
                    </View>

                    <Pressable style={styles.loginFormButton(isValid)} onPress={handleSubmit}>
                        <Text style={styles.loginFormButtonText}>Log In</Text>
                    </Pressable>

                    <View style={styles.loginFormSignUpContainer}>
                        <Text style={{color: 'black'}}>Don't have an account?{' '}</Text>
                        <TouchableOpacity onPress={() => navigation.push('SignupScreen')}>
                            <Text style={{color: '#6BB0F5'}}>Sign Up</Text>
                        </TouchableOpacity>
                    </View>
                </>
            )}
        </Formik>
    );
};

const styles = StyleSheet.create({
    loginFormInputsContainer: {
        marginVertical: 8,
        borderWidth: 1,
        borderRadius: 4,
    },
    loginFormInputsFields: {
        padding: 12,
    },
    loginFormButton: (isValid) => ({
        borderRadius: 4,
        minHeight: 42,
        backgroundColor: isValid ? '#0096F6' : '#9ACAF7',
        alignItems: 'center',
        justifyContent: 'center',
    }),
    loginFormButtonText: {
        fontSize: 20,
        color: 'white',
    },
    loginFormSignUpContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginTop: 24,
    },
});

export default LoginForm;