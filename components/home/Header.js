import React from 'react';
import {View, StyleSheet, Image, TouchableOpacity, Text} from 'react-native';
import {firebase} from '../../firebase';

const handleSignOut = async () => {
    try {
        await firebase.auth().signOut();
        console.log('signed out successfully...');
    } catch (error) {
        console.log(error)
    }
}

const Header = ({navigation}) => {
    return (
        <View style={styles.container}>
            <TouchableOpacity onPress={handleSignOut}>
                <Image style={styles.logo} source={require('../../assets/images/instagram-text-logo.png')}/>
            </TouchableOpacity>

            <View style={styles.iconsContainer}>
                <TouchableOpacity onPress={() => navigation.push('NewPostScreen')}>
                    <Image
                        style={styles.icon}
                        source={require('../../assets/icons/add.png')}
                    />
                </TouchableOpacity>
                <TouchableOpacity>
                    <Image
                        style={styles.icon}
                        source={require('../../assets/icons/like.png')}
                    />
                </TouchableOpacity>
                <TouchableOpacity>
                    <View style={styles.unreadBadge}>
                        <Text style={styles.unreadBadgeText}>10</Text>
                    </View>
                    <Image
                        style={styles.icon}
                        source={require('../../assets/icons/message.png')}
                    />
                </TouchableOpacity>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
        marginHorizontal: 20,
        marginTop: 20,
        // backgroundColor: 'grey'
    },
    iconsContainer: {
        flexDirection: 'row',
    },
    logo: {
        width: 100,
        height: 60,
        resizeMode: 'contain',
    },
    icon: {
        width: 30,
        height: 30,
        marginLeft: 20,
        resizeMode: 'contain',
    },
    unreadBadge: {
        backgroundColor: 'red',
        position: 'absolute',
        left: 22,
        bottom: 20,
        width: 25,
        height: 18,
        borderRadius: 25,
        alignItems: 'center',
        justifyContent: 'center',
        zIndex: 100,
    },
    unreadBadgeText: {
        color: 'white',
        fontWeight: '600',
    },
});

export default Header;
