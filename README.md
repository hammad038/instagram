# Instagram - Clone

## 📝 Update FireBase configs to run the project

- Update the firebase configs in file `firebase.js` in project root dir, to run the app.

```
// Import the functions you need from the SDKs you need
import firebase from 'firebase';

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "<GET_YOUR_CONFIGS>",
    authDomain: "<GET_YOUR_CONFIGS>",
    projectId: "<GET_YOUR_CONFIGS>",
    storageBucket: "<GET_YOUR_CONFIGS>",
    messagingSenderId: "<GET_YOUR_CONFIGS>",
    appId: "<GET_YOUR_CONFIGS>"
};

// Initialize Firebase
!firebase.apps.length ? firebase.initializeApp(firebaseConfig) : firebase.app();

const db = firebase.firestore();

export {firebase, db};
```
