export const Users = [
    {
        user: "Jhon Doe",
        slug: "jhon_doe",
        image: "https://randomuser.me/api/portraits/men/75.jpg"
    },
    {
        user: "Maria Weber",
        slug: "maria_weber01",
        image: "https://randomuser.me/api/portraits/women/57.jpg"
    },
    {
        user: "Hima Malni",
        slug: "malni007",
        image: "https://randomuser.me/api/portraits/women/77.jpg"
    },
    {
        user: "Ni hoi",
        slug: "nihoi.89",
        image: "https://randomuser.me/api/portraits/women/51.jpg"
    },
    {
        user: "Smith Cali",
        slug: "smith_prince",
        image: "https://randomuser.me/api/portraits/men/16.jpg"
    },
    {
        user: "Tina Hilten",
        slug: "tina01",
        image: "https://randomuser.me/api/portraits/women/90.jpg"
    },
    {
        user: "Mark Hen",
        slug: "mark_hen",
        image: "https://randomuser.me/api/portraits/men/26.jpg"
    },
    {
        user: "Jeni Lop",
        slug: "jeni_lop_006",
        image: "https://randomuser.me/api/portraits/women/0.jpg"
    }
];