import React from 'react';
import {View, Text, StyleSheet, Image, TouchableOpacity} from 'react-native';
import FormikPostUploader from './FormikPostUploader';

const AddNewPost = ({navigation}) => {
    return (
        <View style={styles.addNewPostContainer}>
            <Header navigation={navigation}/>
            <FormikPostUploader navigation={navigation} />
        </View>
    );
};

const Header = ({navigation}) => (
    <View style={styles.headerContainer}>
        <TouchableOpacity onPress={() => navigation.goBack()}>
            <Image source={require('../../assets/icons/back.png')} style={styles.headerBack}/>
        </TouchableOpacity>
        <Text style={styles.headerText}>NEW POST</Text>
        <Text></Text>
    </View>
);

const styles = StyleSheet.create({
    addNewPostContainer: {
        marginHorizontal: 12,
        marginVertical: 24,
    },
    headerContainer: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    headerBack: {
        width: 24,
        height: 24,
    },
    headerText: {
        color: 'black',
        fontWeight: '700',
        fontSize: 20,
    },
});

export default AddNewPost;