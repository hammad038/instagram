import React from 'react';
import {View, StyleSheet, Image, Text, TouchableOpacity} from 'react-native';
import {Divider} from 'react-native-elements';
import {firebase, db} from '../../firebase';

const Post = ({post}) => {
    const handleLike = post => {
        const currentLikeStatus = !post.likes_by_users.includes(
            firebase.auth().currentUser.email
        );

        db
            .collection('users')
            .doc(post.owner_email)
            .collection('posts')
            .doc(post.id)
            .update({
                likes_by_users: currentLikeStatus ?
                    firebase.firestore.FieldValue.arrayUnion(firebase.auth().currentUser.email) :
                    firebase.firestore.FieldValue.arrayRemove(firebase.auth().currentUser.email),
            })
            .then(() => console.log('document successfully updated'))
            .catch(error => console.log('error updating document: ', error));
    }

    return (
        <View>
            <Divider width={1} orientation='vertical'/>
            <PostHeader post={post}/>
            <PostImage post={post}/>
            <View style={{marginHorizontal: 12}}>
                <PostFooter handleLike={handleLike} post={post}/>
                <PostLikes post={post}/>
                <PostCaption post={post}/>
                <PostCommentsSection post={post}/>
                <PostComments post={post}/>
            </View>
        </View>
    );
};

const PostHeader = ({post}) => (
    <View style={styles.postHeaderContainer}>
        {console.log(post)}
        <View style={styles.postHeaderInnerContainer}>
            <Image source={{uri: post.profile_picture}} style={styles.postHeaderImage}/>
            <Text style={styles.postHeaderText}>{post.user}</Text>
        </View>
        <Image
            style={styles.postHeaderDots}
            source={require('../../assets/icons/dots.png')}
        />
    </View>
);

const PostImage = ({post}) => (
    <View style={styles.postImageContainer}>
        <Image source={{uri: post.imageUrl}} style={styles.postImage}/>
    </View>
);

const PostFooter = ({handleLike, post}) => (
    <View style={styles.postFooterContainer}>
        <View style={styles.PostFooterIconLeft}>
            <TouchableOpacity onPress={() => handleLike(post)}>
                {console.log(post.likes_by_users.includes(firebase.auth().currentUser.email))}
                <Image
                    style={styles.PostFooterIcon}
                    source={
                        post.likes_by_users.includes(firebase.auth().currentUser.email) ?
                            postFooterIcons[0].likedImageUrl :
                            postFooterIcons[0].imageUrl
                    }
                />
            </TouchableOpacity>
            <PostFooterIcon imgStyle={styles.PostFooterIcon} imgUrl={postFooterIcons[1].imageUrl}/>
            <PostFooterIcon imgStyle={styles.PostFooterIcon} imgUrl={postFooterIcons[2].imageUrl}/>
        </View>
        <View>
            <PostFooterIcon imgStyle={[styles.PostFooterIcon, styles.saveIcon]}
                            imgUrl={postFooterIcons[3].imageUrl}/>
        </View>
    </View>
);

const PostLikes = ({post}) => (
    <View style={styles.postLikesContainer}>
        <Text style={styles.postLikesText}>{post.likes_by_users.length.toLocaleString('en')} likes</Text>
    </View>
);

const PostFooterIcon = ({imgStyle, imgUrl}) => (
    <TouchableOpacity>
        <Image source={imgUrl} style={imgStyle}/>
    </TouchableOpacity>
);

const PostCaption = ({post}) => (
    <View style={styles.postCaptionContainer}>
        <Text>
            <Text style={styles.postCaptionUser}>{post.user} </Text>
            <Text>{post.caption}</Text>
        </Text>
    </View>
);

const PostCommentsSection = ({post}) => (
    <View>
        {!!post.comments.length && (
            <Text style={{color: 'grey'}}>
                View{post.length > 1 ? ' all' : ''} {post.comments.length}{' '}
                {post.comments.length > 1 ? 'comments' : 'comment'}
            </Text>
        )}
    </View>
);

const PostComments = ({post}) => (
    <>
        {
            post.comments.map((comment, index) => (
                <View key={index} style={styles.postCommentsComment}>
                    <Text>
                        <Text style={styles.postCaptionUser}>{comment.user} </Text>
                        <Text>{comment.comment}</Text>
                    </Text>
                </View>
            ))
        }
    </>
);


const styles = StyleSheet.create({
    postHeaderContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginVertical: 10,
        marginHorizontal: 12
    },
    postHeaderInnerContainer: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    postHeaderImage: {
        width: 35,
        height: 35,
        borderRadius: 50,
        borderWidth: 1.6,
        borderColor: '#ff8501',
    },
    postHeaderText: {
        marginLeft: 8,
        fontWeight: '700',
    },
    postHeaderDots: {
        width: 18,
        height: 18,
        resizeMode: 'contain',
    },
    postImageContainer: {
        width: '100%',
        height: 300,
    },
    postImage: {
        height: '100%',
        resizeMode: 'cover',
    },
    postFooterContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginVertical: 8
    },
    PostFooterIconLeft: {
        flexDirection: 'row',
    },
    PostFooterIcon: {
        width: 28,
        height: 28,
        marginRight: 10,
    },
    saveIcon: {
        marginRight: 0,
    },
    postLikesContainer: {
        flexDirection: 'row',
    },
    postLikesText: {
        fontWeight: '700',
    },
    postCaptionContainer: {
        flexDirection: 'row',
        marginBottom: 12,
    },
    postCaptionUser: {
        fontWeight: '700',
    },
    postCommentsComment: {
        flexDirection: 'row',
        marginTop: 3,
    }
});

const postFooterIcons = [
    {
        name: 'Like',
        imageUrl: require('../../assets/icons/like.png'),
        likedImageUrl: require('../../assets/icons/liked.png'),
    },
    {
        name: 'Comment',
        imageUrl: require('../../assets/icons/comment.png'),
    },
    {
        name: 'Share',
        imageUrl: require('../../assets/icons/message.png'),
    },
    {
        name: 'Save',
        imageUrl: require('../../assets/icons/save.png'),
        savedImageUrl: require('../../assets/icons/saved.png'),
    }
];

export default Post;