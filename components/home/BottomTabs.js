import React, {useState} from 'react';
import {View, StyleSheet, Image, TouchableOpacity} from 'react-native';
import {Divider} from 'react-native-elements';

const BottomTabs = ({icons}) => {
    const [activeTab, setActiveTab] = useState('Home');

    const Icon = ({icon}) => (
        <TouchableOpacity onPress={() => setActiveTab(icon.name)}>
            <Image
                source={activeTab === icon.name ? icon.active : icon.inactive}
                style={
                    [
                        styles.bottomTabIcon,
                        icon.name === 'Profile' ? styles.profilePicture() : null,
                        activeTab === 'Profile' && icon.name === activeTab ? styles.profilePicture(activeTab) : null,
                    ]
                }
            />
        </TouchableOpacity>
    );

    return (
        <View style={styles.bottomTabsWrapper}>
            <Divider width={1} orientation='vertical'/>
            <View style={styles.bottomTabIconContainer}>
                {icons.map((icon, index) => (
                    <Icon icon={icon} key={index}></Icon>
                ))}
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    bottomTabsWrapper: {
        position: 'absolute',
        bottom: 0,
        width: '100%',
        zIndex: 1000,
        backgroundColor: 'white',
    },
    bottomTabIconContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginVertical: 8,
        marginHorizontal: 16,
    },
    bottomTabIcon: {
        width: 30,
        height: 30,
    },
    profilePicture: (activeTab = '') => ({
        borderRadius: 50,
        borderColor: '#000000',
        borderWidth: activeTab === 'Profile' ? 2 : 0,
    }),
});

export const bottomTabsIcons = [
    {
        name: 'Home',
        active: require('../../assets/icons/home-active.png'),
        inactive: require('../../assets/icons/home.png'),
    },
    {
        name: 'Search',
        active: require('../../assets/icons/search-active.png'),
        inactive: require('../../assets/icons/search.png'),
    },
    {
        name: 'Reels',
        active: require('../../assets/icons/reels-active.png'),
        inactive: require('../../assets/icons/reels.png'),
    },
    {
        name: 'Shop',
        active: require('../../assets/icons/shop-active.png'),
        inactive: require('../../assets/icons/shop.png'),
    },
    {
        name: 'Profile',
        active: require('../../assets/images/dummy-user.jpg'),
        inactive: require('../../assets/images/dummy-user.jpg'),
    }
];

export default BottomTabs;
