import React from 'react';
import {View, StyleSheet, Image, Text, ScrollView} from 'react-native';
import {Users} from '../../data/users';

const Stories = () => {
    return (
        <View style={{marginBottom: 13}}>
            <ScrollView
                horizontal
                showHorizontalScrollIndicator={false}
            >
                {Users.map((story, index) => (
                    <View key={index}>
                        <Image
                            style={styles.storyImage}
                            source={{uri: story.image}}
                        />
                        <Text style={styles.storyText}>
                            {story.slug.length > 8 ? story.slug.slice(0, 8).toLowerCase() + '...' : story.slug}
                        </Text>
                    </View>
                ))}
            </ScrollView>
        </View>
    );
};

const styles = StyleSheet.create({
    storyImage: {
        width: 70,
        height: 70,
        borderRadius: 50,
        marginLeft: 16,
        borderWidth: 3,
        borderColor: '#ff8501',
    },
    storyText: {
        textAlign: 'center'
    }
});

export default Stories;