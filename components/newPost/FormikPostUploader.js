import React, {useState, useEffect} from 'react';
import {View, Text, StyleSheet, Image, TouchableOpacity, TextInput, Button} from 'react-native';
import * as Yup from 'yup';
import {Formik} from 'formik';
import {Divider} from "react-native-elements";
import validUrl from 'valid-url';
import {firebase, db} from '../../firebase';

const PlaceholderImg = 'https://www.arischonbrun.com/wp-content/uploads/2013/11/dummy-image-landscape-1.jpg';

const uploadPostSchema = Yup.object().shape({
    imgUrl: Yup.string().url().required('Url is required.'),
    caption: Yup.string().required('Caption of post is required.').max(2200, 'Caption has reached the character limit.'),
});

const FormikPostUploader = ({navigation}) => {
    const [thumbnailUrl, setThumbnailUrl] = useState(PlaceholderImg);
    const [currentLoggedInUser, setCurrentLoggedInUser] = useState(null);

    const getUsername = () => {
        const user = firebase.auth().currentUser;
        const unsubscribe =
            db
                .collection('users')
                .where('owner_uid', '==', user.uid)
                .limit(1)
                .onSnapshot(snapshot =>
                    snapshot.docs.map(doc => {
                        setCurrentLoggedInUser({
                            username: doc.data().username,
                            profilePicture: doc.data().profile_picture
                        })
                    })
                );
        return unsubscribe;
    }

    useEffect(() => {
        getUsername();
        console.log(currentLoggedInUser);
    }, []);

    const uploadPostToFirebase = (imgUrl, caption) => {
        const unsubsccribe = db
            .collection('users')
            .doc(firebase.auth().currentUser.email)
            .collection('posts')
            .add({
                imageUrl: imgUrl,
                user: currentLoggedInUser.username,
                profile_picture: currentLoggedInUser.profilePicture,
                owner_uid: firebase.auth().currentUser.uid,
                owner_email: firebase.auth().currentUser.email,
                caption: caption,
                createdAt: firebase.firestore.FieldValue.serverTimestamp(),
                likes_by_users: [],
                comments: [],
            })
            .then(() =>
            navigation.goBack());

        return unsubsccribe;
    }

    return (
        <Formik
            initialValues={{caption: '', imgUrl: ''}}
            onSubmit={(inputValues) => {
                console.log(inputValues);
                console.log('Your post was submitted successfully.');
                uploadPostToFirebase(inputValues.imgUrl, inputValues.caption)
            }}
            validationSchema={uploadPostSchema}
            validateOnMount={true}
        >
            {({handleBlur, handleChange, handleSubmit, values, errors, isValid}) => (
                <>
                    <View style={styles.formContainer}>
                        <Image source={{uri: validUrl.isUri(thumbnailUrl) ? thumbnailUrl : PlaceholderImg}}
                               style={styles.formImageThumbnail}/>

                        <View style={{flex: 1, marginLeft: 8}}>
                            <TextInput
                                placeholder='Write your caption ...'
                                placeholderTextColor='grey'
                                multiline={true}
                                style={styles.formCaptionInput}
                                onChangeText={handleChange('caption')}
                                onBlur={handleBlur('caption')}
                                value={values.caption}
                            />
                            {errors.caption && (
                                <Text style={styles.formErrors}>{errors.caption}</Text>
                            )}
                        </View>
                    </View>

                    <Divider width={1} orientation='vertical'/>

                    <TextInput
                        placeholder='Enter your Image Url ...'
                        placeholderTextColor='grey'
                        style={styles.formImgUrlInput}
                        onChangeText={handleChange('imgUrl')}
                        onBlur={handleBlur('imgUrl')}
                        value={values.imgUrl}
                        onChange={(e) => setThumbnailUrl(e.nativeEvent.text)}
                    />
                    {errors.imgUrl && (
                        <Text style={styles.formErrors}>{errors.imgUrl}</Text>
                    )}

                    <View style={styles.formButton}>
                        <Button title='Share' onPress={handleSubmit} disabled={!isValid}/>
                    </View>
                </>
            )}
        </Formik>
    );
};

const styles = StyleSheet.create({
    formContainer: {
        margin: 20,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    formImageThumbnail: {
        width: 100,
        height: 100,
        borderColor: 'black',
        borderWidth: 1,
    },
    formCaptionInput: {
        color: 'black',
        fontSize: 20,
    },
    formImgUrlInput: {
        fontSize: 14,
        marginTop: 8,
    },
    formErrors: {
        color: 'red',
        fontSize: 10,
    },
    formButton: {
        marginTop: 24,
    }
});

export default FormikPostUploader;