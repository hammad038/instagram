import React from 'react';
import {SafeAreaView, StyleSheet, View, Text, Image} from 'react-native';
import LoginForm from '../components/login/LoginForm';

const LoginScreen = ({navigation}) => {
    return (
        <SafeAreaView style={styles.loginScreenWrapper}>
            <View style={styles.loginScreenContainer}>
                <Image source={require('../assets/images/instagram-logo.png')} style={styles.loginScreenLogo} />
            </View>
            <View>
                <LoginForm navigation={navigation} />
            </View>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    loginScreenWrapper: {
        backgroundColor: 'white',
        flex: 1,
        paddingHorizontal: 12,

    },
    loginScreenContainer: {
        paddingTop: 80,
        paddingBottom: 50,
        alignItems: 'center',
    },
    loginScreenLogo: {
        width: 80,
        height: 80,
    },
});

export default LoginScreen;