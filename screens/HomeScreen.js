import React, {useEffect, useState} from 'react';
import {SafeAreaView, StyleSheet, ScrollView} from 'react-native';
import Header from '../components/home/Header';
import Stories from '../components/home/Stories';
import Post from '../components/home/Post';
import BottomTabs from '../components/home/BottomTabs';
import {bottomTabsIcons} from '../components/home/BottomTabs';
import {Posts} from '../data/posts';
import {db} from '../firebase';

const HomeScreen = ({navigation}) => {
    const [posts, setPosts] = useState([]);

    useEffect(() => {
        db
            .collectionGroup('posts')
            .orderBy('createdAt', 'desc')
            .onSnapshot(snapshot => {
            setPosts(snapshot.docs.map(post => ({
                id: post.id,
                ...post.data()
            })));
        })
    }, []);

    return (
        <SafeAreaView style={styles.container}>
            <Header navigation={navigation} />
            <Stories/>
            <ScrollView style={styles.postWrapper}>
                {posts.length > 0 && posts.map((post, index) => (
                    <Post post={post} key={index}/>
                ))}
            </ScrollView>
            <BottomTabs icons={bottomTabsIcons} />
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        flex: 1,
    },
    postWrapper: {
        marginBottom: 40,
    }
});

export default HomeScreen;